﻿app.service("playerService", function ($http, $q, $location) {
    return ({
        submit: submit,
        getPlayers: getPlayers
    });

    function getPlayers() {
        var deferred = $q.defer();
        var gameId = getId();
        console.log($location);
        $http.post('api/get-info', { gameId: gameId })
            .then(function (response) {
                return deferred.resolve(response.data);
            }, function (response) {
                return deferred.reject();
            });
        return deferred.promise;
    }

    function submit(players, pencePerPoint) {
        var deferred = $q.defer();
        var data = createRound(players, pencePerPoint);

        $http.post('api/calculate', data)
            .then(function (response) {
                return deferred.resolve(response.data);
            }, function () {
                alert("Something is wrong. Please correct it and submit it again");
            });
        return deferred.promise;
    }

    function getId() {
        var urlParams = $location.$$absUrl.split('/');
        return urlParams[urlParams.length - 1];
    }

    function createRound(players, pencePerPoint) {
        return {
            players: players,
            gameId: getId(),
            pencePerPoint: pencePerPoint
        }
    }
});
