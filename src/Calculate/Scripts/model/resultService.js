﻿app.service("resultService", function ($http, $q, $location) {
    return ({
        getGameResult: getGameResult
    });

    function getGameResult() {
        var deferred = $q.defer();
        $http
            .post("api/result", { gameId: getId() })
            .then(function success(response) {
                return deferred.resolve(response.data);
            }), function error(response) {
                return deferred.reject(response);
            };
        return deferred.promise;
    }

    function getId() {
        var urlParams = $location.$$absUrl.split('/');
        return urlParams[urlParams.length - 1];
    }
});
