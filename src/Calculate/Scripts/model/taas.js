﻿app.controller('taasApp', function ($scope, playerService, $location) {
    $scope.newPlayerName = "";

    $scope.players = [];

    function load() {
        playerService.getPlayers()
            .then(function(data) {
                $scope.players = data.Players;
                $scope.pencePerPoint = data.PencePerPoint;
            });
        modifyResultLink();
    }

    load();

    function modifyResultLink() {
        var link = $('#result-link')[0];
        console.log(link);
        link.href = link.href + '/' + getId();
    }

    $scope.pencePerPoint = 5;

    $scope.displayErrorMessage = false;

    $scope.totalPlayers = function () {
        return $scope.players.length;
    }

    $scope.ShowPlayersSection = function() {
        return $scope.players.length != 0;
    };

    $scope.showGetPlayersButton = true;

    $scope.roundMalResult = [];


    function getId() {
        var urlParams = $location.$$absUrl.split('/');
        return urlParams[urlParams.length - 1];
    }

    $scope.pushPlayers = function () {
        $scope.roundMalResult.push($scope.players);
    };

    $scope.addPlayer = function () {
        var player = new Player($scope.newPlayerName);
        if ($scope.players.length < 5) {
            $scope.players.push(player);
            $scope.newPlayerName = "";
        } else {
            $scope.displayErrorMessage = true;
            $scope.newPlayerName = "";
        }
    }

    $scope.UpdateMalSeen = function(index) {
        if ($scope.players[index].GameWon) {
            $scope.players[index].MalSeen = true;
        }
        if ($scope.players[index].Dubliee) {
            $scope.players[index].MalSeen = true;
        }
    };

    $scope.UpdateDubliee = function(index) {
        $scope.players[index].MalSeen = true;
    };

    $scope.TotalMalsForPlayer = [];

    $scope.UpdateGameWon = function(index) {
        $scope.players.forEach(function(a) {
            a.GameWon = false;
        });
        $scope.players[index].MalSeen = true;
        $scope.players[index].GameWon = true;
    };

    $scope.DisplayTotalMal = false;

    $scope.TotalMalCalc = function (data) {
        if (data != null) {
            $scope.TotalMalsForPlayer = data;
            $scope.DisplayTotalMal = true;
        } else {
            alert("Please fill in everything first");
        }
    };

    $scope.HideShowErrorMessage = function() {
        $scope.displayErrorMessage = false;
    }

    $scope.Calculate = function () {
        playerService
            .submit($scope.players, $scope.pencePerPoint)
            .then(function(data) {
                $scope.TotalMalCalc(data);
            });
    };

    $scope.GetGameResult = function() {
        playerService
            .getGameResult()
            .then(function(data) {
                $scope.rounds = data;
            });
    }
});