﻿app.controller('result', function ($scope, resultService, $location) {

    $scope.players = [];
    $scope.rounds = [];
    function load() {
        resultService
            .getGameResult()
            .then(function (data) {
                if (data != null) {
                    $scope.players = data[0].Players.sort(sortPlayers);
                    $scope.rounds = data.forEach(function(round) {
                        round.Players.sort(sortPlayers);
                    });
                }
                console.log($scope.players);
                console.log($scope.rounds);
            });
//        modifyResultLink();
    }

    load();

    function sortPlayers(a, b) {
        return a.Name.localeCompare(b.Name);
    }

//    function modifyResultLink() {
//        var link = $('#result-link')[0];
//        console.log(link);
//        link.href = link.href + '/' + getId();
//    }

});