using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Calculate.Controllers
{
    public class Game 
    {
        [BsonElement("_id")]
        [BsonId]
        public ObjectId Id { get; set; }

        public string GameId { get; set; }

        public List<PlayerSubset> Players { get; set; }

        public decimal? PencePerPoint { get; set; }
    }
}