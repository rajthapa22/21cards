﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace Calculate.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Result()
        {
            return View();
        }

        public ActionResult Create(FormCollection formCollection)
        {
            var gameId = Guid.NewGuid();

            var uri = "mongodb://admin:asdf@ds054118.mongolab.com:54118/marriage_maal";

            var client = new MongoClient(uri);

            var db = client.GetDatabase("marriage_maal");

            int pencePerPoint;
            var players = new List<PlayerSubset>();
            formCollection.AllKeys.ToList().FindAll(x => x != "PencePerPoint").ToList().ForEach(
                x =>
                {
                    if (formCollection[x] != "")
                    {
                        players.Add(new PlayerSubset(formCollection[x]));
                    }
                });

            var collection = db.GetCollection<BsonDocument>("games");

            var game = new BsonDocument
            {
                { "GameId", gameId.ToString()},
                {"PencePerPoint", int.TryParse(formCollection["PencePerPoint"], out pencePerPoint) ? pencePerPoint : 10 }
            };
            var bsonArray = new BsonArray();

            players.ForEach(x =>
            {
                bsonArray.Add(new BsonDocument("Name", x.Name));
            });

            game.Add("Players", bsonArray);

            collection.InsertOne(game);

            return RedirectToAction("Calculate", new RouteValueDictionary {{"id", gameId}});
        }

        public ActionResult Calculate()
        {
            return View();
        }

        /*

                // GET: Home/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
                // GET: Home/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Home/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Home/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Home/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Home/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Home/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        */
    }
}
