using System.Collections.Generic;

namespace Calculate.Controllers
{
    public class PlayerSubset 
    {
        public string Name { get; set; }

        public PlayerSubset(string name)
        {
            Name = name;
        }
    }

    public class Player : PlayerSubset
    {
        public Player(string name) : base(name)
        {
            Mal = 0;
        }

        public int Mal { get; set; }

        public bool GameWon { get; set; }

        public bool MalSeen { get; set; }

        public int TotalPoints { get; set; }

        public bool Dubliee { get; set; }

        public decimal MoneyPoints { get; set; }
    }


    public class Round
    {
        public string GameId { get; set; }
        public List<Player> Players { get; set; }
    }
}