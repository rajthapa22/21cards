﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Calculate.Controllers;

namespace Calculate.Api
{
    public class MallController : ApiController
    {
        [HttpPost]
        [Route("Home/Calculate/api/calculate")]
        public  RoundResult Get(Round round)
        {
            Console.WriteLine(round);

            var resultRound = Calculate(round);
            if (resultRound != null)
            {
                Repository.SaveRound(round);
            }
            return resultRound;
        }


        [HttpPost]
        [Route("Home/Result/api/result")]
        public List<Round> Get(Game game)
        {
            var rounds = Repository.GetGameResult(game.GameId);
            return rounds;
        }

        private RoundResult Calculate(Round round)
        {
            var players = round.Players;

            var gameWonPlayer = players.Find(x => x.GameWon);

            if (gameWonPlayer == null)
            {
                return null;
            }

            var malSeenPlayers = players.FindAll(x => x.MalSeen).ToList();
            var totalMall = malSeenPlayers.Sum(player => player.Mal);

            malSeenPlayers.Remove(gameWonPlayer);

            var malUnSeenPlayers = players.FindAll(x => x.MalSeen == false).ToList();

            if (malSeenPlayers.Count > 0)
            {
                foreach (var player in malSeenPlayers)
                {
                    var totalMalEarned = player.Mal * players.Count;
                    if (player.Dubliee)
                    {
                        player.TotalPoints = totalMalEarned - totalMall;
                    }
                    else
                    {
                        player.TotalPoints = totalMalEarned - (totalMall + 3);
                    }
                    player.MoneyPoints = (player.TotalPoints * round.PencePerPoint)/100;
                }
            }

            if (malUnSeenPlayers.Count > 0)
            {
                foreach (var player in malUnSeenPlayers)
                {
                    var totalPointEarned = -(totalMall + 10);
                    player.TotalPoints = totalPointEarned;
                    player.MoneyPoints = (player.TotalPoints * round.PencePerPoint) / 100;
                }
            }

            int totalForGameWonPlayer = malSeenPlayers.Sum(x => x.TotalPoints);

            totalForGameWonPlayer += malUnSeenPlayers.Sum(x => x.TotalPoints);

            gameWonPlayer.TotalPoints = -(totalForGameWonPlayer);
            gameWonPlayer.MoneyPoints = (gameWonPlayer.TotalPoints * round.PencePerPoint)/100;

            var calculatedList = new List<Player> { gameWonPlayer };

            
            var completeList = calculatedList.Concat(malUnSeenPlayers).Concat(malSeenPlayers).ToList();

            var result = new RoundResult
            {
                Players = completeList,
                TotalMall = totalMall
            };

            return result;
        } 
    }

    public class GameResult
    {
        public List<Player> Players { get; set; }
    }

    public class RoundResult
    {
        public List<Player> Players { get; set; }
        public int TotalMall { get; set; }
    }

    public class GameInfo
    {
        public List<PlayerSubset> Players { get; set; }
        public decimal? PencePerPoint { get; set; }
    }
}
