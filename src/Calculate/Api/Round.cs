using System.Collections.Generic;
using Calculate.Controllers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Calculate.Api
{
    public class Round
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string GameId { get; set; }
        public List<Player> Players { get; set; }
        public decimal PencePerPoint { get; set; }
    }
}