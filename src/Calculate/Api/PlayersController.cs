﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Calculate.Controllers;
using MongoDB.Driver;

namespace Calculate.Api
{
    public class PlayersController : ApiController
    {
        [HttpPost]
        [Route("Home/Calculate/api/get-info")]
        public  GameInfo Get(Game game)
        {
            var gameInfo = Repository.GetPlayers(game.GameId);
            return new GameInfo
            {
                Players = gameInfo.Players,
                PencePerPoint = gameInfo.PencePerPoint
            };
        }
    }


    public static class Repository
    {
        private const string ConnectionString = "mongodb://admin:asdf@ds054118.mongolab.com:54118/marriage_maal";
        private static readonly MongoClient Client = new MongoClient(ConnectionString);
        private static readonly IMongoDatabase Db = Client.GetDatabase("marriage_maal");


        public  static Game GetPlayers(string id)
        {
            var collection = Db.GetCollection<Game>("games");

            var game =collection.FindSync(x => x.GameId == id).Single();

            return game;
        }

        public static void SaveRound(Round round)
        {
            var collection = Db.GetCollection<Round>("rounds");

            collection.InsertOne(round);
        }

        public static List<Round> GetGameResult(string id)
        {
            var collection = Db.GetCollection<Round>("rounds");

            var rounds = collection.Find(x => x.GameId == id).ToList();

            return rounds;

//            var round = rounds.FirstOrDefault();
//            if (round != null)
//            {
//                var gameResult = new GameResult {Players = round.Players};
//
//                gameResult.Players.ForEach(x =>
//                {
//                    decimal totalMoneyWon = 0;
//
//                    rounds.ForEach(y =>
//                    {
//                        var matchedPlayer = y.Players.Find(player => player.Name == x.Name);
//                        totalMoneyWon = totalMoneyWon + matchedPlayer.MoneyPoints;
//                    });
//
//                    x.MoneyPoints = totalMoneyWon;
//                });
//                
//                round.Players.ForEach(x =>
//                {
//                    gameResult.Players.Add(new Player(x.Name));
//                });
//
//                return gameResult;
//            }
//            return new GameResult();
        }
    }
}
